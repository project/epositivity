CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Epositivity is a module that allows payments to be made through the BNL
e-POSitivity gateway.
From any webform it is possible to redirect the user to the payment gateway.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/epositivity

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/epositivity


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Webform - https://www.drupal.org/project/webform


INSTALLATION
------------

 * Install the Epositivity module as you would normally install a contributed
 Drupal module. Visit https://www.drupal.org/node/1897420 for further
 information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to > Administration > Configuration > Web services >
       BNL's e-POSitivity Payment Gateways for configuration.
    3. Navigate to > Administration > Structure > Webforms
    4. Go to Your webform >  Settings > Emails/Handlers > Add Handler and then
       add e-POSitivity Handler.


MAINTAINERS
-----------

 * Pietro Arturo Panetta - https://arturu.it
