<?php

namespace Drupal\epositivity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure epositivity settings.
 */
class EpositivitySettingsForm extends ConfigFormBase {
  /**
   * Module config.
   *
   * @var string Config settings
   */
  const SETTINGS = 'epositivity.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'epositivity_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['test_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate test environment.'),
      '#description' => $this->t('Use the test environment to simulate payments.'),
      '#default_value' => $config->get('test_mode'),
    ];

    /* Production Fieldset */
    $form['production_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Production environment settings.'),
    ];

    $form['production_settings']['storename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Storename'),
      '#default_value' => $config->get('storename'),
      '#description' => $this->t('Also called "Terminal id". Terminal ID provided by the Merchant Service.'),
    ];

    $form['production_settings']['ksig'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ksig'),
      '#default_value' => $config->get('ksig'),
      '#description' => $this->t('Secret key provided by the Merchant Service.'),
    ];

    $form['production_settings']['server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gateway URL'),
      '#default_value' => $config->get('server_url'),
      '#description' => $this->t("Gateway URL provided by the Merchant Service."),
    ];

    /* Test Fieldset */
    $form['test_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Test environment settings.'),
    ];

    $form['test_settings']['storename_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Storename'),
      '#default_value' => $config->get('storename_test'),
      '#description' => $this->t('Also called "Terminal id". Terminal ID provided by the Merchant Service.'),
    ];

    $form['test_settings']['ksig_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ksig'),
      '#default_value' => $config->get('ksig_test'),
      '#description' => $this->t('Secret key provided by the Merchant Service.'),
    ];

    $form['test_settings']['server_url_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gateway URL'),
      '#default_value' => $config->get('server_url_test'),
      '#description' => $this->t("Gateway URL provided by the Merchant Service."),
    ];

    /* Mode */
    $form['type_mode'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Operative mode'),
    ];

    $form['type_mode']['info_type_mode'] = [
      '#type' => 'markup',
      '#markup' => $this->t('POS operational settings'),
    ];

    $form['type_mode']['txntype'] = [
      '#type' => 'select',
      '#options' => [
        'PURCHASE' => $this->t('Movimentazione (PURCHASE)'),
      ],
      '#title' => $this->t('Type of transaction'),
      '#description' => $this->t('The payment gateway can operate in different types of transactions: Movimentazione (PURCHASE), Autorizzazione (AUTH), Conferma (CONFIRM), Credito (CREDIT), Storno (VOID). Type of transactions are described in chapter 5.A of the manual.'),
      '#default_value' => $config->get('txntype'),
    ];

    $form['type_mode']['mode'] = [
      '#type' => 'select',
      '#options' => [
        'payonly' => 'payonly',
      ],
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Type of payment. Fixed parameter. Page 21 of the manual.'),
      '#default_value' => $config->get('mode'),
    ];

    /* Internationalization */
    $form['l10n'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Internationalization'),
    ];

    $form['l10n']['timezone'] = [
      '#type' => 'select',
      '#options' => [
        'CET' => $this->t('CET - Central European Time'),
        'GMT' => $this->t('GMT - Greenwich Mean Time'),
        'EET' => $this->t('EET - Eastern European Time'),
      ],
      '#title' => $this->t('Timezone'),
      '#description' => $this->t('The time zone of the transaction. CET is the time zone in use in Italy.'),
      '#default_value' => $config->get('timezone'),
    ];

    $form['l10n']['language'] = [
      '#type' => 'select',
      '#options' => [
        'IT' => $this->t('IT - Italian'),
        'EN' => $this->t('EN - English'),
        'DE' => $this->t('DE - German'),
      ],
      '#title' => $this->t('Default language'),
      '#description' => $this->t('The payment gateway supports as language: Italian, English, German. If the language is not automatically identified, or the visitor uses a language other than those supported by the gateway, the selected option will be used.'),
      '#default_value' => $config->get('language'),
    ];

    $form['l10n']['currency'] = [
      '#type' => 'select',
      '#options' => [
        'EUR' => $this->t('EUR - Euro'),
        'AED' => $this->t('AED - United Arab Emirates Dirham'),
        'AUD' => $this->t('AUD - Australian Dollar'),
        'BRL' => $this->t('BRL - Brazilian Real'),
        'CAD' => $this->t('CAD - Canadian Dollar'),
        'CHF' => $this->t('CHF - Swiss Franc'),
        'DKK' => $this->t('DKK - Danish Krone'),
        'GBP' => $this->t('GBP - British Pound'),
        'HKD' => $this->t('HKD - Hong Kong Dollar'),
        'JPY' => $this->t('JPY - Japanese Yen'),
        'KWD' => $this->t('KWD - Kuwaiti Dinar'),
        'MXN' => $this->t('MXN - Mexican Peso'),
        'MYR' => $this->t('MYR - Malaysian Ringgit'),
        'NOK' => $this->t('NOK - Norwegian Krone'),
        'RUB' => $this->t('RUB - Russian Ruble'),
        'SAR' => $this->t('SAR - Saudi Rial'),
        'SEK' => $this->t('SEK - Swedish Krona'),
        'SGD' => $this->t('SGD - Singapore Dollar'),
        'THB' => $this->t('THB - Thai Baht'),
        'TWD' => $this->t('TWD - New Taiwan Dollar'),
        'USD' => $this->t('USD - USA Dollar'),
      ],
      '#title' => $this->t('Currency of the transaction'),
      '#description' => $this->t('The currency in which the transaction will be carried out.'),
      '#default_value' => $config->get('currency'),
    ];

    $form['info_url'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h3>URL backend</h3>
        <ul>
          <li>response_success_url: @response_success_url</li>
          <li>response_fail_url: @response_fail_url</li>
          <li>transaction_notification_url: @transaction_notification_url</li>
        </ul>
        ',
        [
          '@response_success_url' => $config->get('response_success_url'),
          '@response_fail_url' => $config->get('response_fail_url'),
          '@transaction_notification_url' => $config->get('transaction_notification_url'),
        ]
      ),
    ];

    $form['info_version'] = [
      '#type' => 'markup',
      '#markup' => $this->t('This module is based on version @version of the AXEPTA e-POSitivity payment gateway', ['@version' => $config->get('gateway_version')]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('test_mode', $form_state->getValue('test_mode'))
      ->set('storename', $form_state->getValue('storename'))
      ->set('ksig', $form_state->getValue('ksig'))
      ->set('server_url', $form_state->getValue('server_url'))
      ->set('storename_test', $form_state->getValue('storename_test'))
      ->set('ksig_test', $form_state->getValue('ksig_test'))
      ->set('server_url_test', $form_state->getValue('server_url_test'))
      ->set('txntype', $form_state->getValue('txntype'))
      ->set('mode', $form_state->getValue('mode'))
      ->set('timezone', $form_state->getValue('timezone'))
      ->set('language', $form_state->getValue('language'))
      ->set('currency', $form_state->getValue('currency'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
