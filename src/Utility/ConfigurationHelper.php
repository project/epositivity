<?php

namespace Drupal\epositivity\Utility;

use Drupal\webform\Entity\WebformSubmission;

/**
 * This class configure the POS.
 */
class ConfigurationHelper {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * Config array.
   *
   * @var array
   * ['module_config'][ object ]
   * ['uuid'][ string ]
   * ['webform_uuid'][ string ]
   * ['sid'][ int ]
   * ['test_mode'][ array ]
   * ['pos'][ array ]
   * ['submission'][ array ]
   * ['response'][ array ]
   */
  private $config = [];

  /**
   * ConfigurationHelper constructor.
   *
   * @param string $uuid
   *   String with the submission uuid in sha256 format.
   * @param int $sid
   *   Submission id.
   */
  public function __construct($uuid, $sid) {
    $this->config['uuid'] = $uuid;
    $this->config['sid'] = $sid;
    $this->config['module_config'] = \Drupal::config('epositivity.settings');
    $this->config['test_mode'] = \Drupal::config('epositivity.settings')->get('test_mode');

    $submission = WebformSubmission::load($sid);
    $this->config['submission'] = $submission->getData();
    $this->config['webform_uuid'] = $submission->uuid();

    $this->setPos();
  }

  /**
   * Return POS configuration.
   *
   * @return array
   *   Array with POS configuration.
   */
  public function get() {
    return $this->config;
  }

  /**
   * Set a POS parameter.
   *
   * @param array $set
   *   Array with new settings.
   */
  public function set(array $set) {
    $this->config = array_replace_recursive($this->config, $set);
  }

  /**
   * Initial settings.
   */
  private function setPos() {
    if ($this->config['test_mode']) {
      $this->config['pos']['storename'] = \Drupal::config('epositivity.settings')->get('storename_test');
      $this->config['pos']['ksig']      = \Drupal::config('epositivity.settings')->get('ksig_test');
      $this->config['pos']['action']    = \Drupal::config('epositivity.settings')->get('server_url_test');
    }
    else {
      $this->config['pos']['storename'] = \Drupal::config('epositivity.settings')->get('storename');
      $this->config['pos']['ksig']      = \Drupal::config('epositivity.settings')->get('ksig');
      $this->config['pos']['action']    = \Drupal::config('epositivity.settings')->get('server_url');
    }

    $this->config['pos']['txntype']  = \Drupal::config('epositivity.settings')->get('txntype');
    $this->config['pos']['timezone'] = \Drupal::config('epositivity.settings')->get('timezone');
    $this->config['pos']['mode']     = \Drupal::config('epositivity.settings')->get('mode');
    $this->config['pos']['currency'] = \Drupal::config('epositivity.settings')->get('currency');
    $this->config['pos']['language'] = \Drupal::config('epositivity.settings')->get('language');

    $baseURL = \Drupal::request()->getSchemeAndHttpHost();

    $this->config['pos']['responseSuccessURL'] =
      $baseURL . \Drupal::config('epositivity.settings')->get('response_success_url') . '/'
      . $this->config['uuid'] . '/'
      . $this->config['sid'];

    $this->config['pos']['responseFailURL'] =
      $baseURL . \Drupal::config('epositivity.settings')->get('response_fail_url') . '/'
      . $this->config['uuid'] . '/'
      . $this->config['sid'];

    $this->config['pos']['transactionNotificationURL'] =
      $baseURL . \Drupal::config('epositivity.settings')->get('transaction_notification_url') . '/'
      . $this->config['uuid'] . '/'
      . $this->config['sid'];
  }

  /**
   * Gateway response after transaction.
   */
  public function addGatewayResponse() {
    $this->config['response'] = [
      "approval_code"     => \Drupal::request()->request->get('approval_code'),
      "MYBANK"            => \Drupal::request()->request->get('MYBANK'),
      "txndate_processed" => \Drupal::request()->request->get('txndate_processed'),
      "accountName"       => \Drupal::request()->request->get('accountName'),
      "timezone"          => \Drupal::request()->request->get('timezone'),
      "ccbin"             => \Drupal::request()->request->get('ccbin'),
      "response_hash"     => \Drupal::request()->request->get('response_hash'),
      "expyear"           => \Drupal::request()->request->get('expyear'),
      "expmonth"          => \Drupal::request()->request->get('expmonth'),
      "response_code_3dsecure" => \Drupal::request()->request->get('response_code_3dsecure'),
      "oid"               => \Drupal::request()->request->get('oid'),
      "paymentID"         => \Drupal::request()->request->get('paymentID'),
      "refnumber"         => \Drupal::request()->request->get('refnumber'),
      "txntype"           => \Drupal::request()->request->get('txntype'),
      "paymentMethod"     => \Drupal::request()->request->get('paymentMethod'),
      "currency"          => \Drupal::request()->request->get('currency'),
      "processor_response_code" => \Drupal::request()->request->get('processor_response_code'),
      "dataCreationOrder" => \Drupal::request()->request->get('dataCreationOrder'),
      "cardnumber"        => \Drupal::request()->request->get('cardnumber'),
      "chargetotal"       => \Drupal::request()->request->get('chargetotal'),
      "terminal_id"       => \Drupal::request()->request->get('terminal_id'),
      "PendingAmount"     => \Drupal::request()->request->get('PendingAmount'),
      "status"            => \Drupal::request()->request->get('status'),
    ];
  }

  /**
   * All gateway status code.
   *
   * @return array
   *   Array with all gateway status code.
   */
  public static function gatewayResponseCode() {

    // The gateway response logs are in Italian,
    // exactly these strings are returned.
    $code['success'] = [
      'IGFS_000' => "TRANSAZIONE OK",
      'IGFS_400' => "STORNO OK",
    ];
    $code['failed'] = [
      'ANNULLATO' => 'ANNULLATO DALL\'UTENTE',
      'IGFS_00051' => "INSTITUTION ID NON PRESENTE.",
      'IGFS_001' => "DESTINATARIO SCONOSCIUTO",
      'IGFS_00155' => "BATCH TRACK ID NON VALIDO.",
      'IGFS_00156' => "BATCH TRACK ID NON UNIVOCO",
      'IGFS_00157' => "STRUMENTO PAGAMENTO NON VALIDO.",
      'IGFS_00158' => "NUMERO CARTA NON NUMERICO",
      'IGFS_00159' => "NUMERO CARTA NON PRESENTE",
      'IGFS_002' => "CARTA SCADUTA",
      'IGFS_00202' => "TENTATIVO HACK RILEVATO",
      'IGFS_00203' => "ACCESSO NON VALIDO:ULITIZZARE METODO POST",
      'IGFS_00253' => "MAXIMUM CREDIT VOLUME EXCEEDED.",
      'IGFS_00254' => "MAXIMUM CARD DEBIT VOLUME EXCEEDED.",
      'IGFS_00255' => "MAXIMUM CARD CREDIT VOLUME EXCEEDED.",
      'IGFS_00256' => "MAXIMUM CARD TRANSACTION COUNT EXCEEDED.",
      'IGFS_00257' => "MAXIMUM TRANSACTION AMOUNT EXCEEDED.",
      'IGFS_00260' => "TRANSACTION DENIED: CREDITS EXCEED CAPTURES",
      'IGFS_00261' => "TRANSACTION DENIED: CAPTURES EXCEED AUTHORIZATIONS",
      'IGFS_003' => "CARTA ERRATA",
      'IGFS_00300' => "INSTITUTION ID NON PRESENTE.",
      'IGFS_00301' => "RISK PROFILE ID NON PRESENTE.",
      'IGFS_00302' => "CODICE VALUTA NON PRESENTE.",
      'IGFS_004' => "CARTA IN BLACK LIST - RITIRARE",
      'IGFS_00451' => "MERCHANT ID NON PRESENTE.",
      'IGFS_00452' => "TERMINAL ID NON PRESENTE.",
      'IGFS_00453' => "TERMINAL ID NON PRESENTE.",
      'IGFS_00454' => "TERMINAL ID NON VALIDO",
      'IGFS_00456' => "TERMINAL ID NON VALIDO",
      'IGFS_005' => "ERRORE DI FORMATO",
      'IGFS_006' => "ERRORE FILE SYSTEM",
      'IGFS_007' => "ERRORE DI COMUNICAZIONE",
      'IGFS_00701' => "IL BATCH NON PROCESSATO",
      'IGFS_00702' => "IL BATCH NON RIAVVIATO",
      'IGFS_00704' => "BATCH ID NON NUMERICO",
      'IGFS_00705' => "BATCH ID NON PRESENTE",
      'IGFS_008' => "AUTORIZZAZIONE NEGATA",
      'IGFS_009' => "RITIRARE CARTA",
      'IGFS_00950' => "DIRECTORY BATCH UPLOAD NON PRESENTE",
      'IGFS_00951' => "DIRECTORY BATCH DOWNLOAD NON PRESENTE",
      'IGFS_00952' => "NOME DIRECTORY ARCHIVIAZIONE BATCH NON PRESENTE",
      'IGFS_010' => "MERCHANT NON ABILITATO",
      'IGFS_01000' => "TRANSAZIONE NEGATA DAL RISCHIO",
      'IGFS_011' => "CONTATTARE ACQUIRER",
      'IGFS_014' => "MERCHANT NON CONVENZIONATO",
      'IGFS_015' => "ACQUIRER NON GESTITO",
      'IGFS_016' => "CARTA IN RANGE NEGATIVO O STRANIERA",
      'IGFS_018' => "CARTA INESISTENTE",
      'IGFS_020' => "CARTA INVALIDA",
      'IGFS_021' => "CODICE MERCHANT ERRATO",
      'IGFS_029' => "DATA SCADENZA ERRATA",
      'IGFS_030' => "FONDI INSUFFICIENTI",
      'IGFS_032' => "IMPORTO NON VALIDO",
      'IGFS_033' => "TRANSAZIONE ORIGINALE NON TROVATA",
      'IGFS_083' => "ERRORE CIFRATURA TRANSAZIONE",
      'IGFS_085' => "CODICE DIVISA ERRATO",
      'IGFS_086' => "MALFUNZIONAMENTO SISTEMA",
      'IGFS_087' => "ACQUIRER NON RAGGIUNGIBILE",
      'IGFS_088' => "MANCATA RISPOSTA DA ACQUIRER",
      'IGFS_091' => "MALFUNZIONAMENTO SISTEMA ACQUIRER",
      'IGFS_092' => "TRANSAZIONE SCONOSCIUTA",
      'IGFS_093' => "CONFERMA GIA' PRESENTE",
      'IGFS_094' => "CREDITO GIA' PRESENTE",
      'IGFS_095' => "STORNO PER NOTIFICA INESISTENTE",
      'IGFS_096' => "STORNO PER AUTORIZZAZIONE INESISTENTE",
      'IGFS_097' => "CONFERMA PER AUTORIZZAZIONE INESISTENTE",
      'IGFS_098' => "IMPORTO SUPERIORE AD IMPORTO AUTORIZZATO",
      'IGFS_10000' => "CARATTERI NON VALIDI",
      'IGFS_101' => "MAC ERRATO",
      'IGFS_102' => "SOSPETTA FRODE",
      'IGFS_104' => "CARTA SOGGETTA A RESTRIZIONI",
      'IGFS_107' => "CONTATTARE ISSUER",
      'IGFS_108' => "CONTATTARE ISSUER:CASO SPECIALE",
      'IGFS_112' => "INSERIRE PIN",
      'IGFS_115' => "FUNZIONE NON SUPPORTATA SU CARTA",
      'IGFS_117' => "PIN ERRATO",
      'IGFS_118' => "CONTO NON TROVATO O NON ABILITATO",
      'IGFS_119' => "OPERAZIONE NON PERMESSA AL TITOLARE",
      'IGFS_121' => "SUPERATO LIMITE IMPORTO",
      'IGFS_122' => "ERRORE SICUREZZA",
      'IGFS_123' => "SUPERATO LIMITE FREQUENZA",
      'IGFS_125' => "ACQUIRER NON GESTITO",
      'IGFS_129' => "SOSPETTA FRODE SU CARTA",
      'IGFS_160' => "CARTA PERSA",
      'IGFS_164' => "DATA ANTEC. A BLOCCO CARTA",
      'IGFS_180' => "DATI ERRATI",
      'IGFS_181' => "DATI SENSIBILI ERRATI",
      'IGFS_189' => "BIN IN RANGE NEGATIVO",
      'IGFS_1921' => "3DS:UNABLE TO AUTENTICATE",
      'IGFS_1922' => "3DS:AUTENTICATION ERROR",
      'IGFS_1923' => "3DS:UNABLE TO VERIFY (VERES=U)",
      'IGFS_200' => "RITIRARE CARTA",
      'IGFS_20000' => "DATI MANCANTI",
      'IGFS_20001' => "CODICE AZIONE NON VALIDO",
      'IGFS_20002' => "SESSIONE SCADUTA",
      'IGFS_20003' => "COOKIES NON ABILITATI",
      'IGFS_20006' => "BRAND NON VALIDO",
      'IGFS_20007' => "STATO ORDINE NON VALIDO",
      'IGFS_20010' => "URL INVIO RISPOSTA NON VALIDO",
      'IGFS_20011' => "URL INVIO ERRORE NON VALIDO",
      'IGFS_20012' => "TRACK ID NON VALIDO",
      'IGFS_20013' => "CODICE LINGUA NON VALIDO",
      'IGFS_20014' => "CAMPO UDF NON VALIDO",
      'IGFS_20015' => "NOME CARTA NON VALIDO",
      'IGFS_20016' => "INDIRIZZO NON VALIDO",
      'IGFS_20017' => "CAP NON VALIDO",
      'IGFS_20018' => "CVV2 NON VALIDO",
      'IGFS_20019' => "TRANSACTION ID. NON VALIDO",
      'IGFS_20020' => "CAMPO USER IDENTIFIER NON VALIDO",
      'IGFS_20021' => "CAMPO API VERSION NON VALIDO",
      'IGFS_20022' => "CAMPO SIGNATURE NON VALIDO",
      'IGFS_20023' => "CAMPO PAYMENT ID NON VALIDO",
      'IGFS_20024' => "CODICE AUTORIZZAZIONE MANCANTE",
      'IGFS_20025' => "CAMPO REFERENCE DATA NON VALIDO",
      'IGFS_20027' => "FORMATO RICHIESTA NON VALIDO",
      'IGFS_20028' => "DATI BATCH MANCANTI",
      'IGFS_20029' => "DATI BATCH NON VALIDI",
      'IGFS_20030' => "DIRECTORY DATI BATCH NON VALIDA",
      'IGFS_20031' => "DATI BATCH DUPLICATI",
      'IGFS_20032' => "NOME BATCH FILE NON VALIDO",
      'IGFS_20033' => "DATI BATCH NON TROVATI",
      'IGFS_20034' => "BATCH TRACK ID NON VALIDO",
      'IGFS_20035' => "ORDERID NON VALIDO",
      'IGFS_20036' => "PAN NON VALIDO",
      'IGFS_20037' => "CVV2 NON VALIDO",
      'IGFS_20038' => "EXPIRE DATE NON VALIDA",
      'IGFS_20040' => "MPI VERIFIED NON VALIDA",
      'IGFS_20041' => "MPI AUTHORIZED NON VALIDA",
      'IGFS_20042' => "MPI CAVV NON VALIDA",
      'IGFS_20043' => "MPI XID DATE NON VALIDA",
      'IGFS_20044' => "CAMPO PAYMENT DESCRIPTION NON VALIDO",
      'IGFS_20045' => "PAYMENT INSTRUMENT TOKEN ID ERRATO",
      'IGFS_20046' => "FREE TEXT ERRATO",
      'IGFS_20048' => "ERRORE PROCESSAMENTO PAYMENT INSTRUMENT TOKEN",
      'IGFS_20049' => "TOPUPID ERRATO",
      'IGFS_20050' => "CODICE POSTALE MITTENTE NON VALIDO",
      'IGFS_20051' => "CODICE POSTALE DESTINATARIO NON VALIDO",
      'IGFS_20052' => "CODICE NAZIONE DESTINATARIO NON VALIDO",
      'IGFS_20053' => "IMPORTO SPEDIZIONE NON VALIDO",
      'IGFS_20056' => "IMPORTO TASSE NON VALIDO",
      'IGFS_20057' => "ELENCO ARTICOLI MANCANTE",
      'IGFS_20058' => "CODICE ARTICOLO NON VALIDO",
      'IGFS_20059' => "DESCRIZIONE ARTICOLO NON VALIDO",
      'IGFS_20060' => "NUMERO NON VALIDO",
      'IGFS_20062' => "IMPORTO ARTICOLO NON VALIDO",
      'IGFS_20065' => "NUMERO MASSIMO PRODOTTI SUPERATO",
      'IGFS_20066' => "CODICE NAZIONE MITTENTE NON VALIDO",
      'IGFS_20067' => "NOME DESTINATARIO NON VALIDO",
      'IGFS_20068' => "INDIRIZZO DESTINATARIO NON VALIDO",
      'IGFS_20069' => "CITTA DESTINATARIO NON VALIDA",
      'IGFS_20070' => "STATO DESTINATARIO NON VALIDO",
      'IGFS_20071' => "CODICE VAT NON VALIDO",
      'IGFS_20072' => "NOME FATTURA NON VALIDO",
      'IGFS_20073' => "INDIRIZZO FATTURA NON VALIDO",
      'IGFS_20074' => "CITTA FATTURA NON VALIDA",
      'IGFS_20075' => "STATO FATTURA NON VALIDO",
      'IGFS_20076' => "CODICE POSTALE FATTURA NON VALIDO",
      'IGFS_20077' => "CODICE NAZIONE FATTURA NON VALIDO",
      'IGFS_20078' => "NUMERO FATTURA NON VALIDO",
      'IGFS_20079' => "NOTE AL VENDITORE NON VALIDE",
      'IGFS_20080' => "PAYPASSDATA ERRATO",
      'IGFS_20081' => "PROMOCODE ERRATO",
      'IGFS_20082' => "ACCNTNAME ERRATO",
      'IGFS_20083' => "AUTHCODE ERRATO",
      'IGFS_20084' => "BUYERNAME ERRATO",
      'IGFS_20085' => "BUYERACCNT ERRATO",
      'IGFS_20086' => "IPBUYER ERRATO",
      'IGFS_20090' => "TRANSAZIONE CANCELLATA DALL'UTENTE",
      'IGFS_20100' => "ERRORE NOTIFICA MERCHANT",
      'IGFS_208' => "CARTA PERSA",
      'IGFS_209' => "CARTA RUBATA",
      'IGFS_800' => "TERMINALE NON ABILITATO",
      'IGFS_801' => "BANCA SELEZIONATA ERRATA",
      'IGFS_802' => "TENTATIVI PIN ESAURITI",
      'IGFS_803' => "CODICE TERMINALE ERRATO",
      'IGFS_804' => "CHIAVE DISALLINEATA",
      'IGFS_805' => "ERRORE CIFRATURA",
      'IGFS_807' => "TERMINALE CHIUSO",
      'IGFS_808' => "TERMINALE NON CHIUSO",
      'IGFS_809' => "ERRORE SEQUENZA",
      'IGFS_810' => "TERMINALE NON RICONOSCIUTO",
      'IGFS_811' => "TERMINALE BLOCCATO",
      'IGFS_812' => "TERMINALE CHIUSO FORZ.",
      'IGFS_813' => "OPERAZIONE NON PERMESSA",
      'IGFS_814' => "TRANSAZIONE IN CORSO",
      'IGFS_815' => "CARTA BLOCCATA",
      'IGFS_888' => "IN ATTESA DI COMPLETAMENTO",
      'IGFS_90000' => "DATABASE ERROR",
      'IGFS_90005' => "TIMESTAMP ERRATO.",
      'IGFS_902' => "TRANSAZIONE NON VALIDA",
      'IGFS_903' => "REINVIARE TRANSAZIONE",
      'IGFS_907' => "EMITTENTE NON ADERENTE",
      'IGFS_908' => "DESTINAZIONE NON TROVATA",
      'IGFS_909' => "ERRORE DI SISTEMA",
      'IGFS_910' => "SISTEMA ISSUER NON ATTIVO",
      'IGFS_911' => "TIME OUT",
      'IGFS_912' => "ISSUER NON RAGGIUNGIBILE",
      'IGFS_913' => "TRANSAZIONE DUPLICATA",
      'IGFS_934' => "NOTIFICA RIFIUTATA",
      'IGFS_990' => "STRUMENTO PAGAMENTO NON ATTIVO",
      'IGFS 888' => "PENDING TRANSACTION,EXECUTE CHECKOUT",
      'IGFS 20050' => "CODICE POSTALE MITTENTE NON VALIDO",
      'IGFS_20051' => "CODICE POSTALE RIF. DI SPEDIZIONE NON VALIDO",
      'IGFS_20052' => "CODICE NAZIONE RIF. 0 1 SPEDIZIONE NON VALIDO",
      'IGFS 20053' => "IMPORTO SPEDIZIONE NON VALIDO",
      'IGFS_20056' => "IMPORTO TASSE NON VALIDO",
      'IGFS_20057' => "VALORE NUMERO ARTICOLI NON PRESENTE",
      'IGFS 20058' => "CODICE ARTICOLO NON VALIDO",
      'IGFS_20059' => "DESCRIZIONE ARTICOLO NON VALIDO",
      'IGFS_20060' => "VALORE NUMERO ARTICOLI NON VALIDO",
      'IGFS 20062' => "IMPORTO ARTICOLO NON VALIDO",
      'IGFS_20065' => "SUPERATO LIMITE NUMERO ARTICOLI",
      'IGFS_20066' => "CODICE NAZIONE MITTENTE NON VALIDO",
      'IGFS 20067' => "RIFERIMENTO SPEDIZIONE NON VALIDO",
      'IGFS_20068' => "INDIRIZZO RIF. SPEDIZIONE NON VALIDO",
      'IGFS_20069' => "CITTA' RIF. SPEDIZIONE NON VALIDO",
      'IGFS_20070' => "STATO RIF. SPEDIZIONE NON VALIDO",
      'IGFS_20071' => "CODICE FISCALE NON VALIDO",
      'IGFS_20072' => "CODICE POSTALE RIF. DI FATTURAZIONE NON VALIDO",
      'IGFS_20073' => "CODICE NAZIONE RIF. FATTURAZIONE NON VALIDO",
      'IGFS_20074' => "RIFERIMENTO FATTURAZIONE NON VALIDO",
      'IGFS_20075' => "INDIRIZZO RIF. FATTURAZIONE NON VALIDO",
      'IGFS_20076' => "CITTA' RIF FATTURAZIONE NON VALIDO",
      'IGFS 20077' => "STATO RIF. FATTURAZIONE NON VALIDO",
    ];
    return $code;
  }

}
