<?php

namespace Drupal\epositivity\Utility;

use Drupal\webform\WebformException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Yaml\Yaml;

/**
 * Helper class epositivity methods.
 */
class PaymentHelper {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * PaymentHelper settings.
   *
   * @var ConfigurationHelper
   */
  private $config;

  /**
   * PaymentHelper constructor.
   *
   * @param string $uuid
   *   String with the submission uuid in sha256 format.
   * @param int $sid
   *   Webform submission id.
   */
  public function __construct($uuid, $sid) {
    $this->config = new ConfigurationHelper($uuid, $sid);
    $this->checkAccess($uuid, $this->config->get()['webform_uuid']);
  }

  /**
   * Make pay data to be sent to the template page.
   *
   * @return array
   *   PaymentHelper settings.
   *
   * @throws \Exception
   */
  public function getPayData() {
    $this->checkChargeTotal();

    $txndatetime = date("Y:m:d-H:i:s");
    $set['pos']['txndatetime'] = $txndatetime;
    $set['pos']['hash'] = $this->getHashCode($txndatetime);
    $this->config->set($set);

    $this->insertTransaction();

    return $this->config->get();
  }

  /**
   * Response verification algorithm.
   *
   * @return array
   *   Array of data with the response to be sent to the template page.
   *
   * @throws \exception
   */
  public function verifyResponse() {
    $this->config->addGatewayResponse();

    if ($this->config->get()['response']["processor_response_code"] == NULL) {
      throw new AccessDeniedHttpException();
    }

    // Load transaction from database.
    $storedTransaction = $this->selectTransaction();

    // If the transaction is already approved or failed.
    // For example, if the page is reloaded, or if the gateway
    // has approved the transaction through the notification URL.
    $storedStatus = $storedTransaction->status;
    if ($storedStatus == 'success' || $storedStatus == 'failed') {
      return $this->getResponse($storedStatus);
    }

    // If the transaction is pending.
    if ($storedTransaction->status == 'pending') {
      // Check hash.
      if ($this->config->get()['response']['response_hash'] != $this->verifyHashCode($storedTransaction->txndatetime)) {
        throw new AccessDeniedHttpException();
      }

      // Check if a success code is returned.
      $successCode = array_key_exists(
        $this->config->get()['response']["processor_response_code"],
        ConfigurationHelper::gatewayResponseCode()['success']
      );

      $responseStatus = ($successCode) ? 'success' : 'failed';

      // Add response message to log.
      $processorResponseMessage['response']['processor_response_message'] =
        ConfigurationHelper::gatewayResponseCode()[$responseStatus][$this->config->get()['response']["processor_response_code"]];
      $this->config->set($processorResponseMessage);

      // Update stored transaction.
      $this->updateTransaction($responseStatus, $this->config->get()['response']);

      return $this->getResponse($responseStatus);
    }
    throw new AccessDeniedHttpException();
  }

  /**
   * Make response array data.
   *
   * @param string $status
   *   Status of the transaction.
   *
   * @return array
   *   Array of data with the response to be sent to the template page.
   */
  private function getResponse($status) {
    return [
      'status' => $status,
      'message' => ($status == 'success') ? $this->t('Successful transaction') : $this->t('Failed transaction.'),
      'log' => ($this->config->get()['test_mode'] == 1) ? $this->config->get()['response'] : '',
    ];
  }

  /**
   * Check if the uuid passed (sha256) matches the stored uuid (in raw format).
   *
   * @param string $uuid
   *   String with the submission uuid in sha256 format.
   * @param string $webformUuid
   *   String with the submission uuid in row mode.
   */
  private function checkAccess($uuid, $webformUuid) {
    $uuidCalc = hash('sha256', $webformUuid);
    if ($uuid != $uuidCalc) {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * Check chargetotal field.
   */
  private function checkChargeTotal() {

    if (!isset($this->config->get()['submission']['chargetotal'])) {
      $errorMessage = $this->t('The chargetotal field is mandatory');
      \Drupal::logger('epositivity')->error($errorMessage);
      throw new WebformException($errorMessage);
    }

    if (is_object($this->config->get()['submission']['chargetotal'])) {
      $value = (float) $this->config->get()['submission']['chargetotal']->__toString();
    }
    else {
      $value = (float) $this->config->get()['submission']['chargetotal'];
    }

    if ($value <= 0) {
      $errorMessage = $this->t('The chargetotal field must be greater than zero');
      \Drupal::logger('epositivity')->error($errorMessage);
      throw new WebformException($errorMessage);
    }

    $set['submission']['chargetotal'] = number_format($value, 2, '.', '');
    $this->config->set($set);
  }

  /**
   * Hash code to send to the gateway.
   *
   * @param string $txndatetime
   *   Date and time of the transaction (AAAA:MM:GG-hh:mm:ss).
   *
   * @return string
   *   String with the hash code.
   */
  private function getHashCode($txndatetime) {
    $strToHash =
      $this->config->get()['pos']['storename'] .
      $txndatetime .
      $this->config->get()['submission']['chargetotal'] .
      $this->config->get()['pos']['currency'] .
      $this->config->get()['pos']['ksig'];
    $ascii = bin2hex($strToHash);
    return sha1($ascii);
  }

  /**
   * Hash verification sent by the gateway.
   *
   * @param string $txndatetime
   *   Date and time of the transaction (AAAA:MM:GG-hh:mm:ss).
   *
   * @return string
   *   String with the hash code.
   */
  private function verifyHashCode($txndatetime) {
    $strToHash =
      $this->config->get()['pos']['ksig'] .
      $this->config->get()['response']['approval_code'] .
      $this->config->get()['response']['chargetotal'] .
      $this->config->get()['response']['currency'] .
      $txndatetime .
      $this->config->get()['pos']['storename'];
    $ascii = bin2hex($strToHash);
    return sha1($ascii);
  }

  /**
   * Store transaction in db.
   *
   * @throws \Exception
   */
  private function insertTransaction() {
    $transaction = [
      'sid'           => $this->config->get()['sid'],
      'txndatetime'   => $this->config->get()['pos']['txndatetime'],
      'status'        => 'pending',
      'test'          => isset($this->config->get()['test_mode']) &&
      $this->config->get()['test_mode'] == 1 ? 1 : 0,
    ];

    $connection = \Drupal::database();
    $connection->insert('epositivity_transaction')->fields($transaction)->execute();
  }

  /**
   * Read transaction from db.
   *
   * @return array
   *   Array with one transaction.
   *
   * @throws \exception
   */
  private function selectTransaction() {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * FROM {epositivity_transaction} WHERE sid = :sid", [':sid' => $this->config->get()['sid']]);
    $result = $query->fetchAll();

    if (count($result) == 1) {
      return $result[0];
    }
    $errorMessage = $this->t('Error. Duplicated Transaction @sid', ['@sid' => $this->config->get()['sid']]);
    \Drupal::logger('epositivity')->error($errorMessage);
    throw new \exception($errorMessage);
  }

  /**
   * Update transaction in db.
   *
   * @param string $status
   *   Status of the transaction.
   * @param array $log
   *   Gateway log.
   *
   * @return bool
   *   Return TRUE if transaction is updated.
   *
   * @throws \exception
   */
  private function updateTransaction($status, array $log) {
    $gateway_log = Yaml::dump($log);

    $connection = \Drupal::database();
    $num_updated = $connection->update('epositivity_transaction')
      ->fields([
        'status' => $status,
        'gateway_log' => $gateway_log,
      ])
      ->condition('sid', $this->config->get()['sid'], '=')
      ->execute();

    if ($num_updated == 1) {
      return TRUE;
    }
    else {
      $errorMessage = $this->t('Error. Duplicated Transaction @sid', ['@sid' => $this->config->get()['sid']]);
      \Drupal::logger('epositivity')->error($errorMessage);
      throw new \exception($errorMessage);
    }
  }

}
