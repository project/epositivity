<?php

namespace Drupal\epositivity\Controller;

use Drupal\epositivity\Utility\PaymentHelper;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class PaymentController manage pay, response and notification user page.
 *
 * @package Drupal\epositivity\Controller
 */
class PaymentController {

  /**
   * Prepare the data to be sent to the user's payment page.
   *
   * @param string $uuid
   *   String with the submission uuid in sha256 format.
   * @param int $sid
   *   Webform submission id.
   *
   * @return array
   *   Data to be sent to the template page.
   *
   * @throws \Exception
   */
  public function pay($uuid, $sid) {
    $helper = new PaymentHelper($uuid, $sid);
    $data = $helper->getPayData();

    return [
      '#theme' => 'epositivity-pay',
      '#submission' => $data['submission'],
      '#pos' => $data['pos'],
    ];
  }

  /**
   * User's payment response page (positive or negative)
   *
   * @param string $uuid
   *   String with the submission uuid in sha256 format.
   * @param int $sid
   *   Webform submission id.
   *
   * @return array
   *   Data to be sent to the template page.
   *
   * @throws \exception
   */
  public function response($uuid, $sid) {
    $helper = new PaymentHelper($uuid, $sid);
    $response = $helper->verifyResponse();

    return [
      '#theme' => 'epositivity-response',
      '#response' => $response,
    ];
  }

  /**
   * Notification response (positive or negative)
   *
   * @param string $uuid
   *   String with the submission uuid in sha256 format.
   * @param int $sid
   *   Webform submission id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   *
   * @throws \exception
   */
  public function notification($uuid, $sid) {
    $helper = new PaymentHelper($uuid, $sid);
    $response = $helper->verifyResponse();

    return new JsonResponse($response);
  }

}
