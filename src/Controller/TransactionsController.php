<?php

namespace Drupal\epositivity\Controller;

/**
 * Admin page with report.
 */
class TransactionsController {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * Returns a transactions page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function all() {

    $db = \Drupal::database();
    $query = $db->select('epositivity_transaction', 'et');
    $query->join('webform_submission', 'ws', 'et.sid = ws.sid');
    $query
      ->fields('et')
      ->fields('ws')
      ->orderBy('et.etid', 'DESC');
    $results = $query->execute()->fetchAll();

    return [
      '#title' => $this->t("Transactions of AXEPTA e-POSitivity payment gateways"),
      '#results' => $results,
      '#theme' => 'epositivity-admin-transactions',
    ];
  }

}
