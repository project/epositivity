<?php

namespace Drupal\epositivity\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "epositivity_handler",
 *   label = @Translation("e-POSitivity"),
 *   category = @Translation("Payment Gateways"),
 *   description = @Translation("Redirect the user to the payment page."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class EpositivityHandler extends WebformHandlerBase {

  /**
   * Redirect the user to the payment page.
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    $form_state->setRedirect('epositivity.pay', [
      'uuid' => hash('sha256', $webform_submission->uuid()),
      'sid' => $webform_submission->id(),
    ]);

  }

}
